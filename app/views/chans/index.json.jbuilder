json.array!(@chans) do |chan|
  json.extract! chan, :id
  json.url chan_url(chan, format: :json)
end
