class ChansController < ApplicationController
  before_action :set_chan, only: [:show, :edit, :update, :destroy]

  # GET /chans
  # GET /chans.json
  def index
    @chans = Chan.all
  end

  # GET /chans/1
  # GET /chans/1.json
  def show
  end

  # GET /chans/new
  def new
    @chan = Chan.new
  end

  # GET /chans/1/edit
  def edit
  end

  # POST /chans
  # POST /chans.json
  def create
    @chan = Chan.new(chan_params)

    respond_to do |format|
      if @chan.save
        format.html { redirect_to @chan, notice: 'Chan was successfully created.' }
        format.json { render :show, status: :created, location: @chan }
      else
        format.html { render :new }
        format.json { render json: @chan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /chans/1
  # PATCH/PUT /chans/1.json
  def update
    respond_to do |format|
      if @chan.update(chan_params)
        format.html { redirect_to @chan, notice: 'Chan was successfully updated.' }
        format.json { render :show, status: :ok, location: @chan }
      else
        format.html { render :edit }
        format.json { render json: @chan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /chans/1
  # DELETE /chans/1.json
  def destroy
    @chan.destroy
    respond_to do |format|
      format.html { redirect_to chans_url, notice: 'Chan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_chan
      @chan = Chan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def chan_params
      params.fetch(:chan, {})
    end
end
