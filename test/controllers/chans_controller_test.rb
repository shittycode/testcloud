require 'test_helper'

class ChansControllerTest < ActionController::TestCase
  setup do
    @chan = chans(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:chans)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create chan" do
    assert_difference('Chan.count') do
      post :create, chan: {  }
    end

    assert_redirected_to chan_path(assigns(:chan))
  end

  test "should show chan" do
    get :show, id: @chan
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @chan
    assert_response :success
  end

  test "should update chan" do
    patch :update, id: @chan, chan: {  }
    assert_redirected_to chan_path(assigns(:chan))
  end

  test "should destroy chan" do
    assert_difference('Chan.count', -1) do
      delete :destroy, id: @chan
    end

    assert_redirected_to chans_path
  end
end
